local composer = require "composer" 
local scene = composer.newScene()
local widget = require("widget")
local _H = display.contentHeight
local _W = display.contentWidth

local sfondo1=display.newImageRect("img/sfondokart.jpg",1366, 768)
sfondo1.x=_H/2
sfondo1.y=_W/2

function scene:create( event )
   local sceneGroup = self.view

   sceneGroup:insert(sfondo1)

   local function options()
     composer.gotoScene( "options", "slideLeft", 400 )
   end

   local function go()
     composer.gotoScene( "go", "slideLeft", 400 )
   end

   local function highScore()
     composer.gotoScene( "highScore", "slideLeft", 400 )
   end

   --local sfondo=display.newImageRect("img/titolo.png",316,80)
   --sfondo.x = _H/2 -75
   --sfondo.y =  _W/2
   --sceneGroup:insert(sfondo)

   local optionsButton=widget.newButton {
   width = 100,
   height = 40,
   defaultFile = "img/options.png",
   onRelease = options
   }
   optionsButton.x = _H/2 +20
   optionsButton.y =  _W/2 - 160
   sceneGroup:insert(optionsButton)

   local goButton=widget.newButton {
   width = 195,
   height = 80,
   defaultFile = "img/play.png",
   onRelease = go
   }
   goButton.x = _H/2 - 70
   goButton.y =  _W/2 + 115
   sceneGroup:insert(goButton)

   local highScoresButton=widget.newButton {
   width = 195,
   height = 80,
   defaultFile = "img/scores.png",
   onRelease = highScore
   }
   highScoresButton.x = _H/2 - 70
   highScoresButton.y = _W/2 + 260
   sceneGroup:insert(highScoresButton)
   
end

function scene:show( event )

end

function scene:hide( event )
     
end

function scene:destroy( event )
   

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
