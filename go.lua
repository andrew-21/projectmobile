local composer = require( "composer" )
local scene = composer.newScene()
local widget = require("widget")
local _H = display.contentCenterX
local _W = display.contentCenterY

local sfondo1=display.newImageRect("img/sfondokart.jpg",1366, 768)
sfondo1.x=_H/2+140
sfondo1.y=_W/2+30

function scene:create( event )
   local sceneGroup = self.view
   sceneGroup:insert(sfondo1)

--[[local function scrollListener( event )
  local phase = event.phase
  local direction = event.direction
  if (event.limitReached) then
    if "right" == direction then
      print("Reached right Limit")
    elseif "left" == direction then
      print("Reached left Limit")

    end
  end
  return true
end

local scrollView = widget.newScrollView({
  left = 0,
  top = 0,
  width = _W+100,
  height = _H+200,
  scrollWidth = 400,
  scrollHeight = 600,
  horizontalScrollDisabled = false,
  verticalScrollDisabled = true,
  listener = scrollListener,
}
)

   local car=display.newImageRect("imgGo/carblack.png",50,50)
   car.x=_W - 200
   car.y=_H
   scrollView:insert(car)

   local car1=display.newImageRect("imgGo/carblack1.png",50,50)
   car1.x=_W
   car1.y=_H
   scrollView:insert(car1)
--]]

local function menu()
     composer.gotoScene( "menu", "slideRight", 400 )
end

local function gioco()
     composer.gotoScene( "gioco", "slideLeft", 400 )
end

   local goButton=widget.newButton {
   width = 195,
   height = 80,
   defaultFile = "img/play.png",
   onRelease = gioco
   }
   goButton.x = _H
   goButton.y =  _W + 200
   sceneGroup:insert(goButton)

   local quitButton=widget.newButton {
   width = 100,
   height = 30,
   defaultFile = "img/quit.png",
   onRelease = menu
   }
   quitButton.x = _H/2 - 25
   quitButton.y =  _W/2 - 140
   sceneGroup:insert(quitButton)
end

function scene:show( event )
    local sceneGroup = self.view
end

function scene:hide( event )
    local sceneGroup = self.view
end

function scene:destroy( event )
    local sceneGroup = self.view
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
