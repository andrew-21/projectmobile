local composer = require( "composer" )
local scene = composer.newScene()
local widget = require("widget")
local _H = display.contentCenterX
local _W = display.contentCenterY

local sfondo1=display.newImageRect("img/sfondokart.jpg",1366, 768)
sfondo1.x=_H/2+140
sfondo1.y=_W/2+30

function scene:create( event )
   local sceneGroup = self.view

   sceneGroup:insert(sfondo1)

   local function menu()
     composer.gotoScene( "menu", "slideRight", 400 )
   end

   local myText = display.newText( "Volume", _H/2+100, _W/2+110, 240, 300, native.systemFont, 70)
   myText:setFillColor( 255, 0.5, 0.5 )
   sceneGroup:insert(myText)

   local quitButton=widget.newButton {
   width = 100,
   height = 30,
   defaultFile = "img/quit.png",
   onRelease = menu
   }
   quitButton.x = _H/2 - 25
   quitButton.y =  _W/2 - 140
   sceneGroup:insert(quitButton)


   local piuButton=widget.newButton {
   width = 80,
   height = 30,
   defaultFile = "img/piu.png",
   onRelease = menu
   }
   piuButton.x = _H/2 + 80
   piuButton.y =  _W/2 + 75 
   sceneGroup:insert(piuButton)

   local menoButton=widget.newButton {
   width = 80,
   height = 30,
   defaultFile = "img/meno.png",
   --onRelease = menu
   }
   menoButton.x = _H/2 + 80
   menoButton.y =  _W/2 + 125
   sceneGroup:insert(menoButton)
   
end
function scene:show( event )
  
  local sceneGroup = self.view

end

function scene:hide( event )
  
  local sceneGroup = self.view

end

function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
