---------------------------------------------------------------------------------
--
-- main.lua
--
---------------------------------------------------------------------------------

-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )
local sheetData {
	width = 128,
	height = 128,
	numFrame = 10,
	sheetContentWidth = 750,
	sheetContentHeight = 1334
}

local mySheet = graphics.newImageSheet(sheetData)
local frame1 = display.newImage("Circuito/road01.png", 1)
local frame2 = display.newImage("Circuito/road02.png", 2)
local frame3 = display.newImage("Circuito/road03.png", 3)
local frame4 = display.newImage("Circuito/road04.png", 4)
local frame5 = display.newImage("Circuito/road05.png", 5)
local frame6 = display.newImage("Circuito/road06.png", 6)
local frame7 = display.newImage("Circuito/road07.png", 7)
local frame8 = display.newImage("Circuito/road08.png", 8)
local frame9 = display.newImage("Circuito/road09.png", 9)
local frame10 = display.newImage("Circuito/road10.png", 10)

local sequenceData = {
	
	{ name = "normalRun",
	  frames = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
	  time = 800,
	  loopCount = 0,
	  loopDirection = "forward"
	}

local animation = display.newSprite( mySheet, sequenceData )
}