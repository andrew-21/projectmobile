local composer = require( "composer" )
local scene = composer.newScene()
local widget = require("widget")
local rotateAbout = require ("rotazione")
local _H = display.contentCenterX
local _W = display.contentCenterY

function scene:create( event )
   local sceneGroup = self.view

   local function menu()
     composer.gotoScene( "menu", "slideRight", 400 )
   end

   local function scrollCircuit( self, event )
	    	self.x = self.x - self.speed
   end
   
   local auto = display.newImage("imgGioco/carblack.png")
   auto.x=_H
   auto.y=_W
   auto.speed = 2
   sceneGroup:insert(auto)
   auto.enterFrame = scrollCircuit
   Runtime:addEventListener("enterFrame", auto)

   local function rotation()
   	 rotateAbout(auto,auto.x,auto.y)
   end

   local frecciaButton=widget.newButton {
   width = 100,
   height = 30,
   defaultFile = "imgGioco/frecciaS.png",
   onPress = rotation,
   onRelease = scrollCircuit
   }
   frecciaButton.x = _H/2 - 25
   frecciaButton.y =  _W/2 + 360
   sceneGroup:insert(frecciaButton)

   local quitButton=widget.newButton {
   width = 100,
   height = 30,
   defaultFile = "img/quit.png",
   onRelease = menu
   }
   quitButton.x = _H/2 - 25
   quitButton.y =  _W/2 - 140
   sceneGroup:insert(quitButton)

end
function scene:show( event )
    local sceneGroup = self.view
end

function scene:hide( event )
    local sceneGroup = self.view
end

function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene
