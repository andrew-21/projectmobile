local function vecScale( x, y, s )
	return x *s, y *s
end

local function angle2Vec( angle )
	local screenAngle = math.rad( -(angle+360))
	return - math.cos(screenAngle), math.sin(screenAngle)
end

local function rotateAbout( obj, x, y, params )
	x = x or obj.x
	y = y or obj.y
	params = params or {}

	local pathRadius = params.pathRadius or 0
	obj._pathRot = params.startA or 0
	local endA = params.endA or (obj._pathRot - 90)
	local time = params.time or 2000
	local myEasing = params.myEasing or easing.linear
	local debugEn = params.debugEn

	local vx,vy = angle2Vec(obj._pathRot)
	vx, vy = vecScale(vx, vy, pathRadius)
	obj.x = x + vx
	obj.y = y + vy

	obj.onComplete = function ( self )
		Runtime:removeEventListener("enterFrame", self)
	end

	obj.enterFrame = function ( self )
		local vx, vy = angle2Vec(self._pathRot)
		vx, vy = vecScale(vx, vy, pathRadius)
		self.x = x + vx
		self.y = y + vy
		self.rotation = self._pathRot

		if(debugEn) then
			local tmp = display.newCircle(self.parent, self.x, self.y, 1)
			tmp:toBack()
	    end
	end
	Runtime:addEventListener("enterFrame", obj)

	transition.to(obj, {_pathRot = endA, time = time, transition = myEasing, onComplete = obj})
end

return rotateAbout